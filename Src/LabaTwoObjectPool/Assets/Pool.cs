using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] private PoolConfig _poolConfig;
    [SerializeField] private List<GameObject> _poolObjects;
    [SerializeField] private Transform _object;

    [SerializeField] private GameObject _cube;

    private void Awake()
    {
        InicializationPool();
    }

    private void InicializationPool()
    {
        foreach (PoolObject obj in _poolConfig.PoolObject)
        {
            for (int i = 0; i < obj.Number; i++)
            {
                GameObject instatiat = Instantiate(obj.Prefabs, _object);
                _poolObjects.Add(instatiat);
                instatiat.SetActive(false);
            }
        }
    }

    public GameObject GetPoolItem(string tag)
    {
        foreach (GameObject obj in _poolObjects)
        {
            if (obj.tag.Equals(tag) && !obj.gameObject.activeInHierarchy)
            {
                obj.transform.position = Input.mousePosition;

                Debug.Log("activated");
                obj.SetActive(true);
                return obj;
            }
        }
        return null;
    }
}
