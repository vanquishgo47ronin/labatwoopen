using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerImpules : MonoBehaviour
{
    [SerializeField] private Bullite _bulite;
    [SerializeField] private Camera _mainCamera;
    [SerializeField] public Pool _pool;

    [SerializeField] Transform _spaunObjects;
    [SerializeField] GameObject _obj;

    [SerializeField] private float _miniSecond = 0f;

    [SerializeField] private float _second = 0;

    void Update()
    {
        ClikeMouse();
        _miniSecond += 1;
    }

    private void ClikeMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _mainCamera.ScreenToWorldPoint(Input.mousePosition);


            _spaunObjects.position = Input.mousePosition;


            _obj = _pool.GetPoolItem("PoolItemGreen");

            _bulite.Impulse();
        }
    }
}
