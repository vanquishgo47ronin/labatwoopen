using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Box", fileName = "SpaunObjects")]
public class PoolConfig : ScriptableObject
{
    public List<PoolObject> PoolObject;
}

[System.Serializable]
public class PoolObject
{
    [SerializeField] private int _number;
    [SerializeField] private GameObject _prefab;


    public int Number
    {
        get
        {
            return _number;
        }
    }

    public GameObject Prefabs => _prefab;
}
